use thiserror::Error;

#[derive(Error, Debug)]
pub enum SchwaerzError {
    #[error("io operation failed")]
    IoError(#[from] std::io::Error),
    #[error("regex init failed")]
    RegexInitError(#[from] regex::Error),
}

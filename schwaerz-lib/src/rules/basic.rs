use super::Rule;

#[derive(Debug)]
pub struct BasicRule {
    pub target: String,
    pub replacement: String,
}

impl Rule for BasicRule {
    fn process(&self, input: &str) -> String {
        let target = &self.target.to_owned();

        input.replace(target, &self.replacement)
    }
}

#[cfg(test)]
mod tests {
    use crate::rules::test_util::test_rule;

    use super::*;

    #[test]
    fn match_works() {
        test_rule(
            BasicRule {
                target: "SebRut".to_string(),
                replacement: "[HANDLE]".to_string(),
            },
            &"user: sebrut, handle: SebRut, syl: seb rut".to_string(),
            "user: sebrut, handle: [HANDLE], syl: seb rut",
        );
    }
}

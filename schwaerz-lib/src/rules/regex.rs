use regex::Regex;

use crate::error::SchwaerzError;

use super::Rule;

#[derive(Debug)]
pub struct RegexRule {
    pub regex: Regex,
    pub replacement: String,
}

impl RegexRule {
    pub fn new(regex: &str, replacement: String) -> Result<Self, SchwaerzError> {
        let regex = Regex::new(regex)?;

        Ok(RegexRule { regex, replacement })
    }
}

impl Rule for RegexRule {
    fn process(&self, input: &str) -> String {
        self.regex.replace_all(input, &self.replacement).to_string()
    }
}
#[cfg(test)]
mod tests {
    use crate::rules::test_util::test_rule;

    use super::*;

    #[test]
    fn match_works() {
        test_rule(
            RegexRule::new(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}", "[IP]".to_string()).unwrap(),
            &"03.10.2023 - 127.0.0.1 - 03.11.2023".to_string(),
            "03.10.2023 - [IP] - 03.11.2023",
        );
    }
}

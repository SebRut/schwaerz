use std::io::{BufRead, BufWriter, Write};

use crate::{error::SchwaerzError, rules::Rule};

pub fn process(
    mut input: impl BufRead,
    output: impl Write,
    rules: Vec<Box<dyn Rule>>,
) -> Result<(), SchwaerzError> {
    let mut line = String::new();
    let mut writer = BufWriter::new(output);

    while input.read_line(&mut line)? != 0 {
        for rule in &rules {
            line = rule.process(&line)
        }

        writer.write_all(line.as_bytes())?;
        line.clear();
    }

    Ok(())
}

#[cfg(test)]
mod tests {

    use crate::rules::{basic::BasicRule, regex::RegexRule, Rule};

    use super::process;

    #[test]
    fn process_correctly() {
        let input_str = b"line one with sebrut or something
line two no match
three sebrut three
four to the floor";
        let input = input_str as &[u8];

        let rules = vec![
            Box::new(BasicRule {
                target: "sebrut".to_string(),
                replacement: "[BLANK]".to_string(),
            }) as Box<dyn Rule>,
            Box::new(BasicRule {
                target: "one".to_string(),
                replacement: "1".to_string(),
            }) as Box<dyn Rule>,
            Box::new(RegexRule::new(r"\d", "DIGIT".to_string()).unwrap()) as Box<dyn Rule>,
        ];

        let mut output = Vec::new();

        process(input, &mut output, rules).unwrap();

        let output_str = String::from_utf8(output).unwrap();

        assert_eq!(
            output_str,
            "line DIGIT with [BLANK] or something
line two no match
three [BLANK] three
four to the floor"
        );
    }
}

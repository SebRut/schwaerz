pub mod basic;
pub mod regex;

pub trait Rule {
    fn process(&self, input: &str) -> String;
}

#[cfg(test)]
pub(crate) mod test_util {
    use super::Rule;

    pub(crate) fn test_rule(rule: impl Rule, input: &str, expected: &str) {
        // when
        let result = rule.process(input);

        // then
        assert_eq!(result, expected)
    }
}

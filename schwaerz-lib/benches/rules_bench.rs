use criterion::{criterion_group, criterion_main, BenchmarkId, Criterion};
use schwaerz_lib::rules::{basic::BasicRule, regex::RegexRule, Rule};

pub fn basic_rule_bench(c: &mut Criterion) {
    let base_line = "my long line with some test data for user sebrut as a benchmark test";
    let rule = BasicRule {
        target: "sebrut".to_string(),
        replacement: "[USER]".to_string(),
    };

    let mut group = c.benchmark_group("rule_basic");

    for factor in 1..11 {
        let line: &str = &base_line.repeat(factor);

        group.bench_with_input(BenchmarkId::from_parameter(factor), line, |b, l| {
            b.iter(|| rule.process(l));
        });
    }

    group.finish();
}

pub fn regex_rule_bench(c: &mut Criterion) {
    let base_line = "my long line with some test data for 192.168.2.1 as a benchmark test";
    let rule = RegexRule::new(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}", "[IP]".to_string()).unwrap();

    let mut group = c.benchmark_group("rule_regex");

    for factor in 1..11 {
        let line: &str = &base_line.repeat(factor);

        group.bench_with_input(BenchmarkId::from_parameter(factor), line, |b, l| {
            b.iter(|| rule.process(l));
        });
    }

    group.finish();
}

criterion_group!(benches, basic_rule_bench, regex_rule_bench);
criterion_main!(benches);

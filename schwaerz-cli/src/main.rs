use std::io::IsTerminal;

use cli::run_cli;
use config::parse_config;
use eyre::Result;
use stdin::run_from_stdin;

mod cli;
mod common;
mod config;
mod stdin;

fn main() -> Result<()> {
    let config = parse_config()?;

    if std::io::stdin().is_terminal() {
        run_cli(config)
    } else {
        run_from_stdin(config)
    }
}

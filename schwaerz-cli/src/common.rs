use schwaerz_lib::rules::{basic::BasicRule, regex::RegexRule, Rule};

use crate::config::{Config, RuleEntry};
use eyre::Result;

pub(crate) fn generate_rules(config: Config) -> Vec<Box<dyn Rule>> {
    config
        .rules
        .into_iter()
        .flat_map(map_entry_to_rule)
        .collect()
}

fn map_entry_to_rule(entry: RuleEntry) -> Result<Box<dyn Rule>> {
    let rule = match entry {
        RuleEntry::Basic {
            target,
            replacement,
        } => Box::new(BasicRule {
            target,
            replacement,
        }) as Box<dyn Rule>,
        RuleEntry::Regex { regex, replacement } => {
            Box::new(RegexRule::new(&regex, replacement)?) as Box<dyn Rule>
        }
    };

    Ok(rule)
}

use directories::ProjectDirs;
use eyre::eyre;
use eyre::Result;
use serde::Deserialize;

pub(crate) fn parse_config() -> Result<Config> {
    let config_file = ProjectDirs::from("de", "sebrut", "schwaerz")
        .ok_or_else(|| eyre!("creating project dirs failed"))?
        .config_dir()
        .join("config");

    let raw_config = config::Config::builder()
        .add_source(
            config::File::with_name(
                config_file
                    .to_str()
                    .ok_or_else(|| eyre!("converting config_file path to &str failed"))?,
            )
            .required(false),
        )
        .build()?;

    let config = raw_config.try_deserialize::<Config>()?;

    Ok(config)
}

#[derive(Debug, Deserialize)]
pub(crate) struct Config {
    pub(crate) rules: Vec<RuleEntry>,
}

#[derive(Debug, Deserialize)]
#[serde(tag = "type")]
pub(crate) enum RuleEntry {
    Basic { target: String, replacement: String },
    Regex { regex: String, replacement: String },
}

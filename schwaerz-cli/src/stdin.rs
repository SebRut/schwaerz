use std::io::BufReader;

use eyre::Result;
use schwaerz_lib::processor;

use crate::{common::generate_rules, config::Config};

pub(crate) fn run_from_stdin(config: Config) -> Result<()> {
    let input = BufReader::new(std::io::stdin());
    let output = std::io::stdout();
    let rules = generate_rules(config);

    processor::process(input, output, rules)?;

    Ok(())
}

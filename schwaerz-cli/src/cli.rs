use std::{fs::File, io::BufReader, path::PathBuf};

use crate::{common::generate_rules, config::Config};
use clap::Parser;
use eyre::Result;
use schwaerz_lib::processor;

#[derive(Parser, Debug)]
struct Args {
    input: PathBuf,
    output: PathBuf,
}

pub(crate) fn run_cli(config: Config) -> Result<()> {
    let args = Args::parse();

    let input_file = File::open(args.input)?;
    let input = BufReader::new(input_file);

    let output = File::create(args.output)?;

    let rules = generate_rules(config);

    processor::process(input, output, rules)?;

    Ok(())
}

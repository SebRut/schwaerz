# Schwaerz - CLI Redaction Tool
Schwaerz is a command-line tool designed for redacting sensitive information in large text files, such as logs. With Schwaerz, you can easily configure redaction rules and perform redaction on text files, helping you protect sensitive data and maintain privacy.

## Installation

Install with `cargo` from source:

```
cargo install --git https://codeberg.org/SebRut/schwaerz.git
```

or from registry

```
cargo install --index https://codeberg.org/SebRut/_cargo-index schwaerz-cli
```

## Usage

You can pipe text into the tool like this and get the redacted text back to stdout:

```
cat logfile | schwaerz-cli
```

Or you can pass the input and output file like this:

```
schwaerz-cli input.txt output.txt
```
## Configuration

Place a `config.yaml` in `$XDG_CONFIG_HOME/schwaerz/` (or your OS equivalent) and add rules like this:

```yaml
rules:
  - type: Basic
    target: "My Corp"
    replacement: "[CORPORATION]"
```

### Supported rule types

#### Basic
performs simple text replacement

##### arguments
  * `target`: the text to find
  * `replacement`: the replacement to use

#### Regex
searches for regex matches

#### arguments
* `regex`: the regex to use
* `replacement`: the replacement to use
